# pct-tasks

A project that uses types and tasks provided by [PCT] to interact with
[OpenEdge] from [Gradle]. It implements a task that creates an
[OpenEdge] database and loads the schema.

See the corresponding [guide](https://grabl.gitlab.io/guides/using-pct-types/).


[Gradle]: https://gradle.org/
[PCT]: https://github.com/Riverside-Software/pct
[OpenEdge]: https://www.progress.com/openedge
