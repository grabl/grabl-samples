# simple

A simplest project, that follows gradle/grabl conventions (that is sources in
`src/main/abl`, tests in `src/test/abl`, etc.) and thus requires almost no
configuration.

See the corresponding [guide](https://grabl.gitlab.io/guides/creating-a-new-abl-project/).
