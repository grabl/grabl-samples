block-level on error undo, throw.

class hello.hello4:
  define public property who as char no-undo get. set.

  constructor public hello4():
    assign who = "world".
  end constructor.

  method public void greet():
    disp "hello" who.
  end method.
end class.
