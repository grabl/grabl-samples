# grabl-samples #

This repository contains sample projects showcasing usage of [grabl] -
the [OpenEdge ABL][OpenEdge] plugin for [Gradle] powered-by [PCT].

The various subfolders represent projects with different needs, from
the simplest requiring almost no configuration to more complex ones.

This code is complimentary to the
[grabl guides](https://grabl.gitlab.io/guides/) - there you can find
step-by-step explanations and more information.

## simple

[simple](simple/) is a simplest project, that follows [gradle]/[grabl]
conventions (that is sources in `src/main/abl`, tests in
`src/test/abl`, etc.) and thus requires almost no configuration.

## custom-paths

[custom-paths](custom-paths/) is a project that follows most
conventions but customises where the source files are and where to put
the compiled r-code.

## custom-compile-tasks

[custom-compile-tasks](custom-compile-tasks/) is a project that
configures compilation in a completely custom manner, it uses
*grabl-base* plugin, and therefore avoids loading any conventions and
instead creates its own compilation tasks.

## pct-tasks

[pct-tasks](pct-tasks/) is a project that uses types and tasks provided
by [PCT] to interact with [OpenEdge] from [Gradle].


[Gradle]: https://gradle.org/
[grabl]: https://grabl.gitlab.io/
[PCT]: https://github.com/Riverside-Software/pct
[OpenEdge]: https://www.progress.com/openedge
