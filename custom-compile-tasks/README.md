# custom-compile-tasks

A project that configures compilation in a completely custom manner, it
uses *grabl-base* plugin, and therefore avoids loading any conventions
and instead creates its own compilation tasks.

See the corresponding [guide](https://grabl.gitlab.io/guides/creating-custom-compile-tasks/).
