# custom-paths

A project that follows most conventions but customises where the source files
are and where to put the compiled r-code.

See the corresponding [guide](https://grabl.gitlab.io/guides/configuring-custom-paths/).
